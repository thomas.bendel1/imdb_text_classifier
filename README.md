# IMDb_text_classifier

Very basic text classification with IMDb-dataset for explaining some fundamentals
Thomas Bendel, in March 2021
thomas.bendel@gmx.de

sample data is used from:

http://ai.stanford.edu/~amaas/data/sentiment/

Andrew L. Maas, Raymond E. Daly, Peter T. Pham, Dan Huang, Andrew Y. Ng, and Christopher Potts. (2011). Learning Word Vectors for Sentiment Analysis. The 49th Annual Meeting of the Association for Computational Linguistics (ACL 2011)
